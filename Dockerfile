# Dockerfile
FROM rocker/tidyverse
RUN apt-get update && apt-get install -y libbz2-dev liblzma-dev libhdf5-dev zlib1g-dev libxml2-dev libssh2-1-dev libgsl0-dev less bc build-essential cmake libncurses-dev curl vim htop
RUN R -e "install.packages('h5');install.packages('BiocManager');BiocManager::install()"
## Commun
RUN mkdir -p softwares
WORKDIR /softwares
# Samtools:
RUN wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 && tar jxf samtools-1.9.tar.bz2 && cd samtools-1.9 && sudo ./configure && sudo make && sudo make install
# Fastqc
RUN wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.8.zip && unzip fastqc_v0.11.8.zip -d . && chmod +x /softwares/FastQC/fastqc
ENV PATH=/softwares/FastQC:"$PATH"
# TranscDecoder
RUN wget https://github.com/TransDecoder/TransDecoder/archive/TransDecoder-v5.5.0.tar.gz -O TransDecoder.tar.gz && tar xf TransDecoder.tar.gz
ENV PATH=/softwares/TransDecoder-TransDecoder-v5.5.0:"$PATH"
# Libraries R
RUN R -e "BiocManager::install('scater');BiocManager::install('tximport');install.packages('mvoutlier');install.packages('reshape2');install.packages('Seurat')"
# DESeq2
RUN R -e "BiocManager::install('DESeq2');BiocManager::install('apeglm')"
# Trimmomatic:
RUN wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.39.zip && unzip Trimmomatic-0.39.zip
ENV PATH=/softwares/Trimmomatic-0.39:"$PATH"
# BWA & pip3 & ssh
RUN sudo apt-get install -y bwa ssh python3-pip
# HISAT
RUN curl -LJO https://cloud.biohpc.swmed.edu/index.php/s/hisat2-220-source/download && unzip hisat2-2.2.0-source.zip && cd hisat2-2.2.0 && make
ENV PATH=/softwares/hisat2-2.2.0:"$PATH"
#PATH_RNASEQ="$PATH_RNASEQ:$PWD/hisat2-2.2.0"
# HTseq-count
RUN sudo apt-get install python && sudo apt install -y python3-pip python-htseq && pip3 install HTSeq
#PATHS
RUN echo """ && #!/usr/bin/env bash && PATH='$PATH' && """ > /softwares/paths.sh
RUN echo "source /softwares/paths.sh" >> /etc/skel/.profile
