if (!requireNamespace("BiocManager", quietly = TRUE))
install.packages("BiocManager")

install.packages("sodium", dependencies = T)
install.packages("openssl", dependencies = T)

#install.packages("nloptr", dependencies=T)
#install.packages("mice", dependencies = T)

install.packages("Hmisc", dependencies = T)

BiocManager::install("DESeq2")

