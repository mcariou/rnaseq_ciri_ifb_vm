#!/bin/bash

# This script is executed on the virtual machine during the *Deployment* phase.
# It is used to apply parameters specific to the current deployment.
# It is executed secondly during a cloud deployement in IFB-Biosphere, after the *Installation* phase.

# General parameters
# INSTANCE_HOSTNAME (in): VM hostname or IP address, gotten from the cloud broker,
#   need to be set otherwise
# APP_URL_SERVICE (out): URL of services, will be pushed to the cloud broker
source /etc/profile.d/ifb.sh
# Selection of docker image
# APP_IMG (in): Image name from Docker hub, or URL from an other registry
APP_IMG="mcariouchiche/rnaseq_ciri:beta"
# Apply configuration from Rstudio-IFB app
APP_NAME="rstudio-ifb"
APP_DIR="/ifb/apprepo/$APP_NAME"
cd ${APP_DIR}
source deploy.sh

